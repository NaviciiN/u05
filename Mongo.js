const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/chat', { useNewUrlParser: true });

const MessageSchema = new mongoose.Schema({
  user: String,
  message: String,
  timestamp: { type: Date, default: Date.now }
});

const Message = mongoose.model('Message', MessageSchema);

app.post('/chat', (req, res) => {
  const message = new Message({
    user: req.body.user,
    message: req.body.message
  });
  message.save((err) => {
    if (err) {
      console.log(err);
      res.status(500).send('Error saving message');
    } else {
      res.send('Message saved');
    }
  });
});
