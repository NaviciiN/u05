const io = require('socket.io')(server);

io.on('connection', (socket) => {
  console.log('User connected');
  
  socket.on('chat message', (msg) => {
    const message = new Message({
      user: msg.user,
      message: msg.message
    });
    message.save((err) => {
      if (err) {
        console.log(err);
        socket.emit('error', 'Error saving message');
      } else {
        io.emit('chat message', message);
      }
    });
  });

  socket.on('disconnect', () => {
    console.log('User disconnected');
  });
});
