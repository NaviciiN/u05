const express = require('express');
const app = express();

app.use(express.static('public'));

app.get('/ads', (req, res) => {
  // Code to serve ads
});

app.post('/chat', (req, res) => {
  // Code to handle chat requests
});

const server = app.listen(3000, () => {
  console.log('Server listening on port 3000');
});
